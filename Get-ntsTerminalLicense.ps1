function Get-ntsTerminalLicense ($Computername) {
    Get-CimInstance -ComputerName $Computername -ClassName Win32_TerminalServiceSetting -Namespace "Root/CIMV2/TerminalServices" | 
        Select-Object LicensingType,@{
            l='LicenseServer';
            e={(Invoke-CimMethod $_ -MethodName GetSpecifiedLicenseServerList).SpecifiedLSList}
        }
}