Invoke-Command -ScriptBlock {

    $dnsParams = @{
        InterfaceIndex = (Get-DnsClientServerAddress | Where-Object {$_.ServerAddresses -match "10\.1\.0\.203"}).Interfaceindex
        ServerAddresses = ("10.8.81.202","10.1.0.203","10.1.0.204")
    }
    
    Get-DnsClientServerAddress -InterfaceIndex $dnsParams.InterfaceIndex | Format-List

} -ComputerName #name