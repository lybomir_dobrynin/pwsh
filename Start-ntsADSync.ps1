function Start-ntsADSyncAll {
	if ($env:username -like "*_da") {
		(Get-ADDomainController -Filter {IsReadOnly -eq $false}).hostname | Foreach-Object { repadmin /syncall $_ (Get-ADDomain).DistinguishedName /AdeP }
	} else {
		Write-Host "__inf: Domain admin account only"
	}
}