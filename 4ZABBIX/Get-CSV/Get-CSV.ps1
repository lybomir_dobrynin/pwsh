switch ($args[0]) {
    'name' {
        $x = [pscustomobject]@{data = @((Get-ClusterSharedVolume).name | Select-Object @{l='{#CSVNAME}';e={$_}})}
        $x.data += [pscustomobject]@{'{#CSVNAME}'="_Total"}
        return ($x | ConvertTo-Json)
    }
    default {
        return ([pscustomobject]@{data = ((Get-ClusterSharedVolume).name | Select-Object @{l='{#CSVPATH}';e={"C:\ClusterStorage\$_"}})}) | ConvertTo-Json
    }
}