function Get-ntsVMHostName ($VMName) {

    $scriptblock = {
        $Path = get-item "HKLM:\SOFTWARE\Microsoft\Virtual Machine\Guest\Parameters" -ErrorAction SilentlyContinue

        if ($Path) {
            return (get-item "HKLM:\SOFTWARE\Microsoft\Virtual Machine\Guest\Parameters").GetValue("HostName")
        } else {
            return "Physical Machine"
        }
    }
    
    switch ($VMName) {
        $env:COMPUTERNAME {
            & $scriptblock
            continue
        }
        ($env:COMPUTERNAME + "." + (Get-CimInstance Win32_ComputerSystem).Domain) {
            & $scriptblock
        }
        default {
            try {
                $null = Test-Connection -ComputerName $VMName -ErrorAction Stop
                Invoke-Command -ScriptBlock $scriptblock -ComputerName $VMName -ErrorAction Stop
            } catch {
                return $Error[0].Exception.Message
            }
        }
    }
}