Get-Process ServerManager | Stop-Process –force

$Servers = (Get-RDServer -ConnectionBroker rdcb01.nts.local).server
if ($?) {$Servers = (Get-RDServer -ConnectionBroker rdcb02.nts.local).server}

$Conf = "$env:USERPROFILE\AppData\Roaming\Microsoft\Windows\ServerManager\ServerList.xml"
$Xml = [xml](Get-Content $Conf)

foreach ($s in $Servers) {
	$AddServer = @($xml.ServerList.ServerInfo)[0].clone()
	$AddServer.name = $s 
	$AddServer.lastUpdateTime = '0001-01-01T00:00:00'
	$AddServer.status = '2'
	$Xml.ServerList.AppendChild($AddServer)
}
$Xml.Save($conf)

Start-Process –filepath $env:SystemRoot\System32\ServerManager.exe