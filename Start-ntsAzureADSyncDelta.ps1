function Start-ntsAzureADSyncDelta {
	if ($env:username -like "*_da") {
		Invoke-command -computername azuread01 -scriptblock {Start-ADSyncSyncCycle -PolicyType Delta}
	} else {
		Write-Host "__inf: Domain admin account only"
	}
}