function Set-ntsDynamicIPAddress ($VMName) {
	try {
		Set-SCVirtualNetworkAdapter -VirtualNetworkAdapter $(Get-SCVirtualMachine -Name $VMName).VirtualNetworkAdapters[0] -IPv4AddressType Dynamic
	} catch {
		return $error[0].Exception.Message
	}
}