function Set-ntsStaticIPAddress ($VMName) {
	$VM = Get-SCVirtualMachine -Name $VMName
	$VMNetwork = Get-SCVMNetwork | Out-Gridview -PassThru -Title 'Select VM Network'
	$IPPool = Get-SCStaticIPAddressPool | Out-GridView -PassThru -Title 'Select IP Address Pool'
	$NIC = Get-SCVirtualNetworkAdapter -VM $VM.Name | Out-Gridview -PassThru -Title 'Select VMs vNIC'
	$IPAddress = Grant-SCIPAddress -GrantToObjectType VirtualNetworkAdapter -GrantToObjectID $VM.VirtualNetworkAdapters[($NIC.SlotID)].ID -StaticIPAddressPool $IPPool -Description $VM.Name
	Set-SCVirtualNetworkAdapter -VirtualNetworkAdapter $VM.VirtualNetworkAdapters[($NIC.SlotID)] -VMNetwork $VMNetwork -IPv4AddressType Static -IPv4Addresses $IPAddress.Address #-PortClassification ""
}