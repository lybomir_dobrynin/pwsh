function Get-ntsVMPerVLAN ($VLAN) {
	@(Get-SCVirtualMachine | Get-SCVirtualNetworkAdapter | where-object {$_.vlanid -eq $VLAN} | select Name,vlanid,IPv4Addresses)

}