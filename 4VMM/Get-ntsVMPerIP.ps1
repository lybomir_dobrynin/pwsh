function Get-ntsVMPerIP ($IPA) {
	@(Get-SCVirtualMachine | Get-SCVirtualNetworkAdapter | where-object {$_.IPv4Addresses -eq $IPA} | select Name,IPv4Addresses)
}