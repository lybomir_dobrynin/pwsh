function Get-ntsVMPerMAC ($MacAddr) {
	@(Get-SCVirtualMachine | Get-SCVirtualNetworkAdapter | where-object {$_.MACAddress -eq $MacAddr} | select Name,MACAddress)
}