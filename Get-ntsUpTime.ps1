function Get-ntsUpTime ($Hostname) {
	(Get-Date) - (Get-CimInstance Win32_OperatingSystem -ComputerName $Hostname).LastBootupTime | Select-Object @{l="Time";e={'{0}:{1}:{2}:{3}' -f $_.Days,$_.Hours,$_.Minutes,$_.Seconds}}
}